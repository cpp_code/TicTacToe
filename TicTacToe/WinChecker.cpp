/**@brief: abstract class for checking for won, lost, or tied games
**/
#include "WinChecker.h"

WinChecker::~WinChecker(){};

WinChecker::WinChecker(){
	_gameStatus = _GAME_CONTINUE;
}

int WinChecker::getGameStatus(){
	return _gameStatus;
}

const int WinChecker::_GAME_CONTINUE = 0;

const int WinChecker::_GAME_WON = 1;

const int WinChecker::_GAME_TIE = 2;