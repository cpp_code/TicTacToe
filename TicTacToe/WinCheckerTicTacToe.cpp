/**@brief: checks for winners of board games
**/
#include "WinCheckerTicTacToe.h"

WinCheckerTicTacToe::~WinCheckerTicTacToe(){}

WinCheckerTicTacToe::WinCheckerTicTacToe(){}

/**@brief: check if the game is over or not
*
* checks if the game is over from a winner or a tie
* 
* @arg board: board to check for status on
* @arg place: last place a piece was placed in
*
* @ret: status of the game
**/
int WinCheckerTicTacToe::checkGameStatus(shared_ptr<Board> &board, int place){
	_gameStatus = _GAME_CONTINUE;

	if(checkUpDown(board, place) || checkLeftRight(board, place) || checkDiagTopLeftLowRight(board, place) || checkDiagTopRightLowLeft(board, place)){
		_gameStatus = _GAME_WON;
	} else if(checkTie(board, place)){
		_gameStatus = _GAME_TIE;
	}

	return _gameStatus;
}

int WinCheckerTicTacToe::getGameContinue() {
	return _GAME_CONTINUE;
}

/**@brief: check for winner vertically from last place piece
*
* uses the position of the last placed piece to see if a player won vertically
*
* @arg board: board used to check status of
* @arg place: place/location of the last placed piece
*
* @ret: status of the game
**/
int WinCheckerTicTacToe::checkUpDown(shared_ptr<Board> &board, int place){
	int indecies[3] = {0, 0, 0};
	indecies[0] = place - 1;

	if(indecies[0] - 3 >= 0){
		indecies[1] = indecies[0] - 3;
		if(indecies[0] - 6 >= 0){
			indecies[2] = indecies[0] - 6;
		} else {
			indecies[2] = indecies[0] + 3;
		}
	} else {
		indecies[1] = indecies[0] + 3;
		indecies[2] = indecies[0] + 6;
	}

	if(board->getSpace(indecies[0]) == board->getSpace(indecies[1]) && board->getSpace(indecies[1]) == board->getSpace(indecies[2])){
		return _GAME_WON;
	} 
	
	return _GAME_CONTINUE;

}

/**@brief: check for winner horizontally from last place piece
*
* uses the position of the last placed piece to see if a player won horizontally
*
* @arg board: board used to check status of
* @arg place: place/location of the last placed piece
*
* @ret: status of the game
**/
int WinCheckerTicTacToe::checkLeftRight(shared_ptr<Board> &board, int place){
	int indecies[3] = {0, 0, 0};
	indecies[0] = place - 1;

	if(place % 3 == 0){
		indecies[1] = indecies[0] - 1;
		indecies[2] = indecies[0] - 2;
	} else if(place % 3 == 2){
		indecies[1] = indecies[0] + 1;
		indecies[2] = indecies[0] - 1;
	} else {
		indecies[1] = indecies[0] + 1;
		indecies[2] = indecies[0] + 2;
	}

	if(board->getSpace(indecies[0]) == board->getSpace(indecies[1]) && board->getSpace(indecies[1]) == board->getSpace(indecies[2])){
		return _GAME_WON;
	} 
	
	return _GAME_CONTINUE;
}

/**@brief: check for winner diagonally down left to right from last place piece
*
* uses the position of the last placed piece to see if a player won diagonally down left to right
*
* @arg board: board used to check status of
* @arg place: place/location of the last placed piece
*
* @ret: status of the game
**/
int WinCheckerTicTacToe::checkDiagTopLeftLowRight(shared_ptr<Board> &board, int place){
	if(place == 1 || place == 5 || place == 9){
		if(board->getSpace(0) == board->getSpace(4) && board->getSpace(4) == board->getSpace(8)){
			return _GAME_WON;
		}
	}
	return _GAME_CONTINUE;
}

/**@brief: check for winner diagonally down right to left from last place piece
*
* uses the position of the last placed piece to see if a player won diagonally down right to left
*
* @arg board: board used to check status of
* @arg place: place/location of the last placed piece
*
* @ret: status of the game
**/
int WinCheckerTicTacToe::checkDiagTopRightLowLeft(shared_ptr<Board> &board, int place){
	if(place == 3 || place == 5 || place == 7){
		if(board->getSpace(2) == board->getSpace(4) && board->getSpace(4) == board->getSpace(6)){
			return _GAME_WON;
		}
	}
	return _GAME_CONTINUE;
}

/**@brief: check for tie from last place piece
*
* uses the position of the last placed piece to see if there is a tie
*
* @arg board: board used to check status of
* @arg place: place/location of the last placed piece
*
* @ret: status of the game
**/
int WinCheckerTicTacToe::checkTie(shared_ptr<Board> &board, int place){
	if(board->getSpace(0) != '1' && board->getSpace(1) != '2' && board->getSpace(2) != '3' && board->getSpace(3) != '4' && board->getSpace(4) != '5' && board->getSpace(5) != '6' && board->getSpace(6) != '7' && board->getSpace(7) != '8' && board->getSpace(8) != '9'){
		return _GAME_TIE;
	} else {
		return _GAME_CONTINUE;
	}
}