/**@brief: board for tic tac toe games
**/
#include "BoardTicTacToe.h"

BoardTicTacToe::~BoardTicTacToe(){}

BoardTicTacToe::BoardTicTacToe() : _spaces{ '1','2', '3', '4', '5', '6', '7', '8', '9' }{}

const int BoardTicTacToe::_ARRAY_TO_BOARD_CONVERTER = 1;

const char BoardTicTacToe::_INT_TO_CHAR_CONVERTER = '0';

const int BoardTicTacToe::_VALID_MAX_LOCATION = 9;

const int BoardTicTacToe::_VALID_MIN_LOCATION = 1;

char BoardTicTacToe::getSpace(int index) {
	return _spaces[index];
}

/**@brief: set board space if the space is free
*
* set the board space based on the index and pieces provided
* only set the space if another users piece is not already occupying that space
*
* @arg index: index of where to place the piece on the board
* @arg piece: piece to be placed on the board
*
* @ret: true if piece was successfully placed 
* @ret: false if piece was not successfully placed
**/
bool BoardTicTacToe:: setSpace(int index, char piece) {
	if (_spaces[index] == (char)(_INT_TO_CHAR_CONVERTER + index + _ARRAY_TO_BOARD_CONVERTER)) {
		_spaces[index] = piece;
		return true;
	} else {
		return false;
	}
}

vector<char> BoardTicTacToe::getSpaces(){
	return _spaces;
}

/**@brief: reset all the spaces to their original values
**/
void BoardTicTacToe::resetSpaces(){
	int count = 0;
	for(auto i = _spaces.begin(); i != _spaces.end(); ++i){
		_spaces[count] = (char)(_INT_TO_CHAR_CONVERTER + count + _ARRAY_TO_BOARD_CONVERTER);
		count++;
	}
	
}

int BoardTicTacToe::getValidMaxLocation(){
	return _VALID_MAX_LOCATION;
}

int BoardTicTacToe::getValidMinLocation() {
	return _VALID_MIN_LOCATION;
}