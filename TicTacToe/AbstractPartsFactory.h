#ifndef _ABSTRACT_PARTS_FACTORY_H_
#define _ABSTRACT_PARTS_FACTORY_H_

#include "WinChecker.h"
#include "PlayerFactory.h"
#include "View.h"
#include "Board.h"
#include <string>
#include <iostream>
#include <memory>

using namespace std;

class AbstractPartsFactory{
public:
	virtual ~AbstractPartsFactory();

	virtual shared_ptr<WinChecker> createWinChecker() = 0;

	virtual shared_ptr<PlayerFactory> createPlayerFactory() = 0;

	virtual shared_ptr<View> createView() = 0;

	virtual shared_ptr<Board> createBoard() = 0;


protected:
	AbstractPartsFactory();

};

#endif