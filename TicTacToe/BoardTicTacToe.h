#ifndef _BOARD_TIC_TAC_TOE_H_
#define _BOARD_TIC_TAC_TOE_H_

#include "Board.h"

class BoardTicTacToe : public Board{
public:
	~BoardTicTacToe();

	BoardTicTacToe();

	char getSpace(int index);

	bool setSpace(int index, char piece);

	vector<char> getSpaces();

	void resetSpaces();

	int getValidMaxLocation();

	int getValidMinLocation();

private:
	vector<char> _spaces;

	static const int _ARRAY_TO_BOARD_CONVERTER;

	static const char _INT_TO_CHAR_CONVERTER;

	static const int _VALID_MAX_LOCATION;

	static const int _VALID_MIN_LOCATION;
};

#endif