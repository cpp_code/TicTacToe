#ifndef _HUMAN_PLAYER_TIC_TAC_TOE_
#define _HUMAN_PLAYER_TIC_TAC_TOE_

#include "Player.h"

class HumanPlayerTicTacToe : public Player{
public:
	HumanPlayerTicTacToe(string name, char piece, int wins, int losses, int ties);

	~HumanPlayerTicTacToe();

	int placePiece(shared_ptr<Board> &board, shared_ptr<View> &view);

private:
	static const int _LOCATION_TO_BOARD_CONVERTER;
};

#endif