/**@brief: cpu player for tic tac toe games
**/
#include "CpuPlayerTicTacToe.h"

CpuPlayerTicTacToe::~CpuPlayerTicTacToe() {};

CpuPlayerTicTacToe::CpuPlayerTicTacToe(string name, char piece, int wins, int losses, int ties):_difficulty(1), Player(name, piece, wins, losses, ties) {}

CpuPlayerTicTacToe::CpuPlayerTicTacToe(string name, char piece, int wins, int losses, int ties, unsigned int playerDifficulty) : _difficulty(playerDifficulty), Player(name, piece, wins, losses, ties) {}

const int CpuPlayerTicTacToe::_LOCATION_TO_BOARD_CONVERTER = 1;

const int CpuPlayerTicTacToe::_ME = 1;

const int CpuPlayerTicTacToe::_OPPONENT = 0;

const map<int, vector<int>> CpuPlayerTicTacToe::_WAYS_TO_WIN = {
	{ 1,{ 1,2,3 } },
	{ 2,{ 4,5,6 } },
	{ 3,{ 7,8,9 } },
	{ 4,{ 1,4,7 } },
	{ 5,{ 2,5,8 } },
	{ 6,{ 3,6,9 } },
	{ 7,{ 3,5,7 } },
	{ 8,{ 1,5,9 } }
};


/**@brief: get player to place their piece on a board
*
* cpu evaluates the board and decides where to place their piece
* once decision is made, the piece is placed
*
* @arg board: board piece is to be placed on
* @arg view: view used to display the board
*
* @ret: location on the board that the piece was placed
**/
int CpuPlayerTicTacToe::placePiece(shared_ptr<Board> &board, shared_ptr<View> &view) {
	int location = 0;

	cout << endl << _name << ":" << endl;
	cout << "Select a space to place your piece: " << _piece << endl;
	view->display(board);
	cout << _name << " is deciding where to move" << endl;

	location = evaluateBoard(board);

	cout << "placing piece in location: " << location << endl;

	return location;
}

/**@brief: evaluate where on the board to place a piece based on difficulty
*
* choose a number within the boards range to place the piece
* if possible, place the players piece at this location
*
* @ret: the location the player chose to place their piece
**/
int CpuPlayerTicTacToe::evaluateBoard(shared_ptr<Board> &board) {
	int location = 0;

	if (_difficulty == 1) {
		location = easyMode(board);
	}
	else if (_difficulty == 2) {
		location = mediumMode(board);
	}
	else if (_difficulty == 3) {
		location = hardMode(board);
	}

	return location;
}


/**@brief: choose a location on the board to place a piece based on a hard difficulty algorithm
*
* if you can win place piece in winning spot
* if opponent can win place piece to block
* otherwise place piece based on the shortest possible moves it will take to win
* if there is no way to win place piece at random
*
*@arg board: board to place piece on
*
*@ret: location the piece was placed at
**/
int CpuPlayerTicTacToe::hardMode(shared_ptr<Board> &board) {
	int location = 0;
	unsigned int shortestSizeRouteForMeToWin = 3;
	unsigned int shortestSizeRouteForOpponentToWin = 3;
	vector<int> shortestRoutesForMeToWin;
	vector<int> shortestRoutesForOpponentToWin;

	if (isFirstMove(board)) {
		_waysLeftForMeToWin = _waysLeftForOpponentToWin = _WAYS_TO_WIN;
	}

	removeUnwinnableRoutes(board, _waysLeftForMeToWin, _ME);
	shortestSizeRouteForMeToWin = getShortestSizeRouteToWin(board, shortestSizeRouteForMeToWin, _waysLeftForMeToWin, _ME);
	getShortestRoutesToWin(shortestSizeRouteForMeToWin, shortestRoutesForMeToWin, _waysLeftForMeToWin);

	if (shortestSizeRouteForMeToWin == 1) {
		location = selectRandomSpaceBasedOnWinningRoutes(shortestRoutesForMeToWin, board);
	}
	else {
		removeUnwinnableRoutes(board, _waysLeftForOpponentToWin, _OPPONENT);
		shortestSizeRouteForOpponentToWin = getShortestSizeRouteToWin(board, shortestSizeRouteForOpponentToWin, _waysLeftForOpponentToWin, _OPPONENT);
		getShortestRoutesToWin(shortestSizeRouteForOpponentToWin, shortestRoutesForOpponentToWin, _waysLeftForOpponentToWin);
		if (shortestSizeRouteForOpponentToWin == 1) {
			location = selectRandomSpaceBasedOnWinningRoutes(shortestRoutesForOpponentToWin, board);
		}
		else if(!shortestRoutesForMeToWin.empty() ){
			location = selectRandomSpaceBasedOnWinningRoutes(shortestRoutesForMeToWin, board);
		}
		else {
			location = placeRandomPiece(board);
		}
	}

	cout << shortestSizeRouteForMeToWin << "me" << endl;
	cout << shortestSizeRouteForOpponentToWin << "opponent" << endl;

	return location;
}

/**@brief: choose a location on the board to place a piece based on a medium difficulty algorithm
*
* place piece based on the shortest possible moves it will take to win
* if there is no way to win place piece at random
*
*@arg board: board to place piece on
*
*@ret: location the piece was placed at
**/
int CpuPlayerTicTacToe::mediumMode(std::shared_ptr<Board> & board)
{
	int location = 0;
	unsigned int shortestSizeRouteToWin = 3;
	vector<int> shortestRoutesToWin;
	int me = 1;

	if (isFirstMove(board)) {
		_waysLeftForMeToWin = _WAYS_TO_WIN;
	}

	removeUnwinnableRoutes(board, _waysLeftForMeToWin, me);
	shortestSizeRouteToWin = getShortestSizeRouteToWin(board, shortestSizeRouteToWin, _waysLeftForMeToWin, me);
	getShortestRoutesToWin(shortestSizeRouteToWin, shortestRoutesToWin, _waysLeftForMeToWin);

	if (shortestRoutesToWin.empty()) {
		location = placeRandomPiece(board);
	}
	else {
		location = selectRandomSpaceBasedOnWinningRoutes(shortestRoutesToWin, board);
	}
	
	return location;
}

/**@brief: select a random space from a vector of possible spaces
*
* @arg shortestRoutesToWin: a vector containing the possible spaces to select to win the game
* @arg board: board to place piece on
*
* @ret: location on the board piece was placed
**/
int CpuPlayerTicTacToe::selectRandomSpaceBasedOnWinningRoutes(std::vector<int> &shortestRoutesToWin, std::shared_ptr<Board> & board)
{
	int location = 0;

	random_device rand_dev;
	mt19937 generator(rand_dev());
	uniform_int_distribution<int> distr(0, (shortestRoutesToWin.size() - 1));

	int index = distr(generator);
	location = shortestRoutesToWin[index];
	board->setSpace((location - 1), _piece);

	return location;
}

/**@brief: get the shortest routes to win
*
* save to a vector all possible locations on a board to choose from that will lead to an equal chance of winning
*
* @arg shortestSizeRouteToWin: shortest number of moves needed to win
* @arg shortestRoutestToWin: locations that can be used to win that have an equal chance of leading to a win
*@arg waysLeftToWin: map of all possible ways left to win
**/
void CpuPlayerTicTacToe::getShortestRoutesToWin(unsigned int shortestSizeRouteToWin, std::vector<int> &shortestRoutesToWin, map<int, vector<int>> &waysLeftToWin)
{
	/*save shortest routes left*/
	for (auto it = waysLeftToWin.begin(); it != waysLeftToWin.end(); ++it) {
		if (it->second.size() == shortestSizeRouteToWin) {
			for (auto vIt = it->second.begin(); vIt != it->second.end(); ++vIt) {
				if (find(shortestRoutesToWin.begin(), shortestRoutesToWin.end(), *vIt) == shortestRoutesToWin.end()) {
					shortestRoutesToWin.push_back(*vIt);
					cout << *vIt;
				}
			}
		}
	}
	cout << "shortest routes" << endl;
}

/**@brief: get the shortest number of moves that it will take to win
*
* @arg board: board for playing the game
* @arg shortestSizeRouteToWin: shortest number of moves that it will take to win
*@arg waysLeftToWin: map of all possible ways left to win
*@arg playerFlag: flag denoting if you are evaluating your chances or the opponents
*
* @ret: shortest number of moves that it will take to win
**/
unsigned int CpuPlayerTicTacToe::getShortestSizeRouteToWin(std::shared_ptr<Board> & board, unsigned int shortestSizeRouteToWin, map<int, vector<int>> &waysLeftToWin, int playerFlag)
{
	if (playerFlag) {
		for (auto it = waysLeftToWin.begin(); it != waysLeftToWin.end();) {//for all free or your spaces ways left to win
			for (auto vIt = it->second.begin(); vIt != it->second.end();) {//loop through spaces
				if (it->second.size() < shortestSizeRouteToWin) {
					shortestSizeRouteToWin = it->second.size();//get shortest size
				}
				if (board->getSpace(*vIt - 1) == _piece) {
					vIt = it->second.erase(vIt);//erase where piece is
					if (it->second.size() < shortestSizeRouteToWin) {
						shortestSizeRouteToWin = it->second.size();//get shortest size
					}
				}
				else {
					++vIt;
				}
			}
			++it;
		}
	}
	else {
		for (auto it = waysLeftToWin.begin(); it != waysLeftToWin.end();) {//for all free or your spaces ways left to win
			for (auto vIt = it->second.begin(); vIt != it->second.end();) {//loop through spaces
				if (it->second.size() < shortestSizeRouteToWin) {
					shortestSizeRouteToWin = it->second.size();//get shortest size
				}
				if (board->getSpace(*vIt - 1) != _piece && board->getSpace(*vIt - 1) != (*vIt + '0')) {
					vIt = it->second.erase(vIt);//erase where piece is
					if (it->second.size() < shortestSizeRouteToWin) {
						shortestSizeRouteToWin = it->second.size();//get shortest size
					}
				}
				else {
					++vIt;
				}
			}
			++it;
		}
	}
	return shortestSizeRouteToWin;
}

/**@brief: remove unwinnable routes
*
* if you cannot win a certain way, remove that way from the list of possiblilities
*
* @arg board: board used for the game
*@arg waysLeftToWin: map of all possible ways left to win
*@arg playerFlag: flag denoting if you are evaluating your chances or the opponents
**/
void CpuPlayerTicTacToe::removeUnwinnableRoutes(std::shared_ptr<Board> & board, map<int, vector<int>> &waysLeftToWin, int playerFlag)
{
	//removes unwinnable routes
	if (playerFlag) {
		for (int i = board->getValidMinLocation(); i <= board->getValidMaxLocation(); i++) {
			if (board->getSpace(i - 1) != (i + '0') && board->getSpace(i - 1) != _piece) {//if the space is not free and is not you
				for (auto it = waysLeftToWin.begin(); it != waysLeftToWin.end();) {//loop through the ways to win
					if (find(it->second.begin(), it->second.end(), i) != it->second.end()) {//remove that way
						waysLeftToWin.erase(it++);
					}
					else {
						++it;
					}
				}
			}
		}
	}
	else {
		for (int i = board->getValidMinLocation(); i <= board->getValidMaxLocation(); i++) {
			if (board->getSpace(i - 1) == _piece) {//if the space is not free and is not you
				for (auto it = waysLeftToWin.begin(); it != waysLeftToWin.end();) {//loop through the ways to win
					if (find(it->second.begin(), it->second.end(), i) != it->second.end()) {//remove that way
						waysLeftToWin.erase(it++);
					}
					else {
						++it;
					}
				}
			}
		}
	}
}

/**@brief: evaluate if the user is taking their first move
*
*@arg board: board used to play the game
*
*@ret: true if it is the first move
*@ret: false if it is not the first move
**/
bool CpuPlayerTicTacToe::isFirstMove(std::shared_ptr<Board> & board)
{
	for (int i = board->getValidMinLocation(); i <= board->getValidMaxLocation(); i++) {
		if (board->getSpace(i - 1) == _piece) {
			return false;
		}
	}
	return true;
}

/**@brief: select a location on the board to place a piece based on an easy algorithm
*
*@arg board: board used for playing
*
*@ret: location that the pice was placed
**/
int CpuPlayerTicTacToe::easyMode(std::shared_ptr<Board> & board)
{
	int location = placeRandomPiece(board);
	return location;
}

/**@brief: place a pice on a board at random
*
*@arg board: board used for playing
*
*@ret: location piece was placed
**/
int CpuPlayerTicTacToe::placeRandomPiece(std::shared_ptr<Board> & board)
{
	int location = 0;
	/* objects needed to generate a random number within a range */
	random_device rand_dev;
	mt19937 generator(rand_dev());
	uniform_int_distribution<int> distr(board->getValidMinLocation(), board->getValidMaxLocation());

	/* generate a random number in the valid range to place a pice */
	do {
		location = distr(generator);
	} while (!board->setSpace((location - _LOCATION_TO_BOARD_CONVERTER), _piece));
	return location;
}