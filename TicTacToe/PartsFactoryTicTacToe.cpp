/**@brief: parts factory for tic tac toe games
*
* used to generate the different parts required for a tic tac toe game
**/
#include "PartsFactoryTicTacToe.h"

PartsFactoryTicTacToe::~PartsFactoryTicTacToe(){}

PartsFactoryTicTacToe::PartsFactoryTicTacToe(){}

shared_ptr<WinChecker> PartsFactoryTicTacToe::createWinChecker(){
	return make_shared<WinCheckerTicTacToe>();
}

shared_ptr<PlayerFactory> PartsFactoryTicTacToe::createPlayerFactory(){
	return make_shared<PlayerFactoryTicTacToe>();
}

shared_ptr<View> PartsFactoryTicTacToe::createView(){
	return make_shared<ViewTicTacToe>();
}

shared_ptr<Board> PartsFactoryTicTacToe::createBoard(){
	return make_shared<BoardTicTacToe>();
}