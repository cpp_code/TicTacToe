#ifndef _WIN_CHECKER_H_
#define _WIN_CHECKER_H_

#include <iostream>
#include <string>
#include "board.h"
#include <memory>

using namespace std;

class WinChecker{
public:
	virtual ~WinChecker();

	int getGameStatus();

	virtual int checkGameStatus(shared_ptr<Board> &board, int place) = 0;

	virtual int getGameContinue() = 0;

protected:
	WinChecker();

	int _gameStatus;

	static const int _GAME_CONTINUE;

	static const int _GAME_WON;

	static const int _GAME_TIE;
};

#endif