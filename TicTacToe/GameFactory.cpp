/**@brief: factory used to generate partsFactory of any game type
**/
#include "GameFactory.h"

GameFactory::~GameFactory(){}

GameFactory::GameFactory(){}

const int GameFactory::_TIC_TAC_TOE_KEY = 1;

const map<unsigned int, string> GameFactory::_GAME_TYPES = {
	{ _TIC_TAC_TOE_KEY, "Tic Tac Toe"}
};

/**@brief: prompts the user of the program to specify which type of game they wish to play
*
*@ret: partsFactory of the game type the user wishes to play
**/
shared_ptr<AbstractPartsFactory> GameFactory::promptGameType(){
	string userInput = " ";
	unsigned int gameType = 0;
	
	while(true){
		cout << endl << "What game do you wish to play?" << endl;
		for(auto const& it : _GAME_TYPES){
			cout <<  it.first << ": " << it.second << endl;
		}
		ws(cin);
		getline(cin, userInput);

		stringstream userStream(userInput);
		if(userStream >> gameType){
			if(_GAME_TYPES.find(gameType) != _GAME_TYPES.end()){
				return generatePartsFactory(gameType);
			}
		}
		cout << "Invalid number, please try again" << endl;
	}
}

/**@brief: generate the parts factory according to the specified gameType
*
*@arg gameType: specifies which type of game the user wants to play
*
*@ret: partsFactory of the game type specified
**/
shared_ptr<AbstractPartsFactory> GameFactory::generatePartsFactory(unsigned int gameType) {
	if(gameType == _TIC_TAC_TOE_KEY){
		cout << "You chose: " << _GAME_TYPES.at(gameType) << endl;
		return make_shared<PartsFactoryTicTacToe>();
	} else {
		cout << "You chose: " << _GAME_TYPES.at(gameType) << endl;
		return make_shared<PartsFactoryTicTacToe>(); 
	}
}