/**@brief: view for displaying tic tac toe boards
**/
#include "ViewTicTacToe.h"

ViewTicTacToe::~ViewTicTacToe(){}

ViewTicTacToe::ViewTicTacToe(){}

/**@brief: display the board properly
*
* @arg board: board to be displayed as a tic tac toe board
**/ 
void ViewTicTacToe::display(shared_ptr<Board> &board){
	int count = 1;
	const vector<char> localSpaces = board->getSpaces();

	cout << endl;
	for(auto i = localSpaces.begin(); i != localSpaces.end(); ++i, ++count){
		cout << *i;
		if(count % 3 != 0){
			cout << '|';
		} else if(count != 9) {
			cout << endl;
			cout << "-----" << endl;
		}
	}
	cout << endl;
}