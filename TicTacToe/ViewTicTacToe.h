#ifndef _VIEW_TIC_TAC_TOE_H_
#define _VIEW_TIC_TAC_TOE_H_

#include "View.h"
#include "BoardTicTacToe.h"
#include <iostream>
#include <string>

class ViewTicTacToe:public View{
public:
	~ViewTicTacToe();

	ViewTicTacToe();

	void display(shared_ptr<Board> &board);

};

#endif