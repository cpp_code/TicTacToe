#ifndef _TURN_ENGINE_H_
#define _TURN_ENGINE_H_

#include "AbstractPartsFactory.h"
#include "PlayerFactory.h"
#include "Player.h"
#include "Board.h"
#include "View.h"
#include "WinChecker.h"
#include <string>
#include <iostream>
#include <map>
#include <memory>
using namespace std;

class TurnEngine{
public:
	~TurnEngine();
	
	TurnEngine(shared_ptr<AbstractPartsFactory> &abstractPartsFactory);
	
	int run();
	
private:
	shared_ptr<PlayerFactory> _playerFactory;

	shared_ptr<Player> _playerOne;

	shared_ptr<Player> _playerTwo;

	shared_ptr<Player> _playerTurn;

	shared_ptr<Board> _board;

	shared_ptr<View> _view;

	shared_ptr<WinChecker> _winChecker;

	static const unsigned int _YES_KEY;

	static const unsigned int _NO_KEY;

	static const unsigned int _PLAYER_ONE_KEY;

	static const unsigned int _PLAYER_TWO_KEY;

	static const int _WINNER_CODE;

	static const map<unsigned int, string> _OPTIONS;

	int promptPlayer();

	void evaluateWinner(int gameStatus);

	void scoreWinnerAndLoser(shared_ptr<Player> &winningPlayer, shared_ptr<Player> &losingPlayer);

	void scoreTie();

	void displayScore();

	void setStartingPlayer();

	bool continuePlaying();

	bool promptPlayAgain();

	bool promptChangePieces();

	void changePieces();
};
#endif