#ifndef _CPU_PLAYER_TIC_TAC_TOE_H
#define _CPU_PLAYER_TIC_TAC_TOE_H

#include "Player.h"
#include <random>
#include <map>

class CpuPlayerTicTacToe : public Player {
	static const map<int, vector<int>> _WAYS_TO_WIN;

	map<int, vector<int>> _waysLeftForMeToWin;

	map<int, vector<int>> _waysLeftForOpponentToWin;

	const unsigned int _difficulty;

	static const int _ME;

	static const int _OPPONENT;

public:
	CpuPlayerTicTacToe(string name, char piece, int wins, int losses, int ties);

	CpuPlayerTicTacToe(string name, char piece, int wins, int losses, int ties, unsigned int playerDifficulty);

	~CpuPlayerTicTacToe();

	int placePiece(shared_ptr<Board> &board, shared_ptr<View> &view);

private:
	static const int _LOCATION_TO_BOARD_CONVERTER;

	int evaluateBoard(shared_ptr<Board> &board);

	int easyMode(std::shared_ptr<Board> & board);

	int mediumMode(std::shared_ptr<Board> & board);

	int selectRandomSpaceBasedOnWinningRoutes(std::vector<int> &shortestRoutesToWin, std::shared_ptr<Board> & board);

	void getShortestRoutesToWin(unsigned int shortestSizeRouteToWin, std::vector<int> &shortestRoutesToWin, map<int, vector<int>> &waysLeftToWin);

	unsigned int getShortestSizeRouteToWin(std::shared_ptr<Board> & board, unsigned int shortestSizeRouteToWin, map<int, vector<int>> &waysLeftToWin, int playerFlag);

	void removeUnwinnableRoutes(std::shared_ptr<Board> & board, map<int, vector<int>> &waysLeftToWin, int playerFlag);

	bool isFirstMove(std::shared_ptr<Board> & board);

	int placeRandomPiece(std::shared_ptr<Board> & board);

	int hardMode(shared_ptr<Board> &board);

};

#endif
