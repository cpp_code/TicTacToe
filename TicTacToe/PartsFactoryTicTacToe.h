#ifndef _PARTS_FACTORY_TIC_TAC_TOE_H_
#define _PARTS_FACTORY_TIC_TAC_TOE_H_

#include "AbstractPartsFactory.h"
#include "WinCheckerTicTacToe.h"
#include "PlayerFactoryTicTacToe.h"
#include "ViewTicTacToe.h"
#include "BoardTicTacToe.h"

class PartsFactoryTicTacToe: public AbstractPartsFactory{
public:
	~PartsFactoryTicTacToe();

	PartsFactoryTicTacToe();

	shared_ptr<WinChecker> createWinChecker();

	shared_ptr<PlayerFactory> createPlayerFactory();

	shared_ptr<View> createView();

	shared_ptr<Board> createBoard();
};

#endif