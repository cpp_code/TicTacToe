/**@brief: human player for tic tac toe games
**/
#include "HumanPlayerTicTacToe.h"

HumanPlayerTicTacToe::~HumanPlayerTicTacToe(){}

HumanPlayerTicTacToe::HumanPlayerTicTacToe(string name, char piece, int wins, int losses, int ties):Player(name, piece, wins, losses, ties){}

const int HumanPlayerTicTacToe::_LOCATION_TO_BOARD_CONVERTER = 1;

/**@brief: get player to place their piece on a board
*
* prompts and gets location from player as to where to place their piece
*
* @arg board: board piece is to be placed on
* @arg view: view used to display the board
*
* @ret: location on the board that the piece was placed
**/
int HumanPlayerTicTacToe::placePiece(shared_ptr<Board> &board, shared_ptr<View> &view){
	int location = 0;
	string userInput = " ";

	while (true) {
		cout << endl << _name << ":"  << endl;
		cout << "Select a space to place your piece: " << _piece << endl;
		view->display(board);
		
		ws(cin);
		getline(cin, userInput);

		// This code converts from string to number safely.
		stringstream userStream(userInput);
		if (userStream >> location){
			if(location <= board->getValidMaxLocation() && location >= board->getValidMinLocation()){
				if(board->setSpace(location - _LOCATION_TO_BOARD_CONVERTER, _piece)){
					break;
				}
			}
		}
		cout << "Invalid number, please try again" << endl;
	}

	return location;
}