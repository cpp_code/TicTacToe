#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Board.h"
#include "View.h"
#include <iostream>
#include <string>
#include <sstream>
#include <memory>
using namespace std;

class Player{
public:
	virtual ~Player();

	void setName(string name);

	string getName();

	void setPiece(char piece);

	char getPiece();

	void setWins(int wins);

	int getWins();

	void setLosses(int losses);

	int getLosses();

	void setTies(int ties);

	int getTies();

	virtual int placePiece(shared_ptr<Board> &board, shared_ptr<View> &view) = 0;

protected:
	Player(string name, char piece, int wins, int losses, int ties);

	string _name;

	char _piece;

	int _wins;

	int _losses;

	int _ties;
};
#endif