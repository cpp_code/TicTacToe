#ifndef _WIN_CHECKER_TIC_TAC_TOE_H_
#define _WIN_CHECKER_TIC_TAC_TOE_H_

#include "WinChecker.h"

class WinCheckerTicTacToe: public WinChecker{
public:
	~WinCheckerTicTacToe();

	WinCheckerTicTacToe();

	int checkGameStatus(shared_ptr<Board> &board, int place);

	int getGameContinue();

private:
	int checkUpDown(shared_ptr<Board> &board, int place);

	int checkLeftRight(shared_ptr<Board> &board, int place);

	int  checkDiagTopLeftLowRight(shared_ptr<Board> &board, int place);

	int checkDiagTopRightLowLeft(shared_ptr<Board> &board, int place);

	int checkTie(shared_ptr<Board> &board, int place);
};

#endif