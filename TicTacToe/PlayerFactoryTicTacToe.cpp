/**@brief: player factory for tic tac toe players
**/
#include "PlayerFactoryTicTacToe.h"

PlayerFactoryTicTacToe::~PlayerFactoryTicTacToe(){}

PlayerFactoryTicTacToe::PlayerFactoryTicTacToe() {}

const vector<char> PlayerFactoryTicTacToe::_VALID_PIECES = { 'x', 'o' };

const unsigned int PlayerFactoryTicTacToe::_VALID_NAME_LENGTH = 20;

const unsigned int PlayerFactoryTicTacToe::_HUMAN_PLAYER_KEY = 1;

const unsigned int PlayerFactoryTicTacToe::_CPU_PLAYER_KEY = 2;

const unsigned int PlayerFactoryTicTacToe::_EASY_KEY = 1;

const unsigned int PlayerFactoryTicTacToe::_MEDIUM_KEY = 2;

const unsigned int PlayerFactoryTicTacToe::_HARD_KEY = 3;

const unsigned int PlayerFactoryTicTacToe::_CHAR_LENGTH = 1;

const string PlayerFactoryTicTacToe::_DEFAULT_PLAYER_NAME = "Player";

const map<unsigned int, string> PlayerFactoryTicTacToe::_VALID_PLAYER_TYPES = {
	{_HUMAN_PLAYER_KEY, "Human Player"},
	{_CPU_PLAYER_KEY, "CPU Player"}
};

const map<unsigned int, string> PlayerFactoryTicTacToe::_DIFFICULTY_LEVELS = {
	{ _EASY_KEY, "Easy" },
	{ _MEDIUM_KEY, "Medium" },
	{_HARD_KEY, "Hard"}
};

/**@brief: generate players of specific types
*
* ask users what type of player they wish to create
* then create that player based on their name and piece they wish to use
*
* @ret: player that was generated
**/
shared_ptr<Player> PlayerFactoryTicTacToe::generatePlayer(){
	unsigned int type = 0;
	string name = _DEFAULT_PLAYER_NAME;
	char piece = {0};
	int winsLossesTies = 0;
	
	type = getPlayerType();
	name = getPlayerName();
	piece = getPlayerPiece();

	/* actual creation of the player */
	cout << "You chose: " << _VALID_PLAYER_TYPES.at(type) << endl;
	if(type == _HUMAN_PLAYER_KEY){
		return make_shared<HumanPlayerTicTacToe>(name, piece, winsLossesTies, winsLossesTies, winsLossesTies);
	}
	else if (type == _CPU_PLAYER_KEY) {
		unsigned int playerDifficulty = chooseDifficulty();
		return make_shared<CpuPlayerTicTacToe>(name, piece, winsLossesTies, winsLossesTies, winsLossesTies, playerDifficulty);
	}
}

/**@brief: choose player difficulty
*
*@ret: player difficulty
**/
unsigned int PlayerFactoryTicTacToe::chooseDifficulty()
{
	string userInput = " ";
	unsigned int playerDifficulty = 0;

	while (true) {
		cout << endl << "What difficulty?" << endl;
		for (auto const& it : _DIFFICULTY_LEVELS) {
			cout << it.first << ": " << it.second << endl;
		}
		ws(cin);
		getline(cin, userInput);

		stringstream userStream(userInput);
		if (userStream >> playerDifficulty) {
			if (_DIFFICULTY_LEVELS.find(playerDifficulty) != _DIFFICULTY_LEVELS.end()) {
				return playerDifficulty;
			}
		}
		cout << "Invalid number, please try again" << endl;
	}
}

/** @brief: get the type of player the user wishes to create
*
* prompt the user and get what player type they wish to create
*
* @ret: player type the user wants to create
**/
unsigned int PlayerFactoryTicTacToe::getPlayerType(){
	string userInput = " ";
	unsigned int playerType = 0;
	
	while(true){
		cout << endl << "What type of player?" << endl;
		for(auto const& it : _VALID_PLAYER_TYPES){
			cout <<  it.first << ": " << it.second << endl;
		}
		ws(cin);
		getline(cin, userInput);

		stringstream userStream(userInput);
		if(userStream >> playerType){
			if (_VALID_PLAYER_TYPES.find(playerType) != _VALID_PLAYER_TYPES.end()) {
				return playerType;
			}
		}
		cout << "Invalid number, please try again" << endl;
	}
}

/** @brief: get the piece the player should use
*
* prompt the user and get what piece the player should use
* only let the user select a piece that has not already been selected
*
* @ret: player piece
**/
char PlayerFactoryTicTacToe::getPlayerPiece(){
	string userInput = " ";

	while (true) {
		cout << "What is the players piece?" << endl;
		
		for(auto i = _VALID_PIECES.begin(); i != _VALID_PIECES.end(); i++){
			if(*i == _VALID_PIECES.back()){
				cout << *i << endl;
			} else {
				cout << *i << ", ";
			}
		}

		ws(cin);
		getline(cin, userInput);
	
		if (userInput.length() == _CHAR_LENGTH) {
			for(auto i = _VALID_PIECES.begin(); i != _VALID_PIECES.end(); i++){
				if(userInput[0] == *i){
					if (find(_pickedPieces.begin(), _pickedPieces.end(), userInput[0]) == _pickedPieces.end()) {
						_pickedPieces.push_back(userInput[0]); //track pieces already picked
						return userInput[0];
					}
				}
			}
		}

		cout << "Invalid character, please try again" << endl;
	}
}

/** @brief: get the name the player should use
*
* prompt the user and get what name the player should use
*
* @ret: player name
**/
string PlayerFactoryTicTacToe::getPlayerName(){
	string userInput = " ";

	cout << "What is the players name?" << endl;
	while(true){
		ws(cin);
		getline(cin, userInput);
		if(userInput.length() <= _VALID_NAME_LENGTH){
			return userInput;
		}
		cout<< "The name: " << userInput << " is too long" << endl;
		cout<< "Please enter a name less than " << _VALID_NAME_LENGTH << " characters long." << endl;
	}
}