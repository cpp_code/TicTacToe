#ifndef _VIEW_H_
#define _VIEW_H_

#include "Board.h"
#include <memory>
using namespace std;

class View{
public:
	virtual ~View();
	virtual void display(shared_ptr<Board> &board) = 0;

protected:
	View();
};

#endif