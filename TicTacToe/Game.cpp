
/**@brief: game class to start board games of any available type
**/
#include <iostream>
#include "GameFactory.h"
#include "AbstractPartsFactory.h"
#include "TurnEngine.h"
#include <memory>

using namespace std;
int main(){

	GameFactory gf;
	shared_ptr<AbstractPartsFactory> apf = gf.promptGameType();
	TurnEngine te(apf);

	te.run();

	return 0;
}
	