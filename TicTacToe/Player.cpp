/**@brief: abstract class for players of different types of different games
**/
#include "Player.h"

Player::~Player(){}

Player::Player(string name, char piece, int wins, int losses, int ties): _name(name), _piece(piece), _wins(wins), _losses(losses), _ties(ties){}

void Player::setName(string name) {
	_name = name;
}

string Player::getName(){
	return _name;
}

void Player::setPiece(char piece){
	_piece = piece;
}

char Player::getPiece(){
	return _piece;
}

void Player::setWins(int wins){
	_wins = wins;
}

int Player::getWins(){
	return _wins;
}

void Player::setLosses(int losses){
	_losses = losses;
}

int Player::getLosses(){
	return _losses;
}

void Player::setTies(int ties){
	_ties = ties;
}

int Player::getTies(){
	return _ties;
}