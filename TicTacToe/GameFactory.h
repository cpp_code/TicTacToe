#ifndef _GAME_FACTORY_H_
#define _GAME_FACTORY_H_

#include "AbstractPartsFactory.h"
#include "PartsFactoryTicTacToe.h"
#include <map>
#include <iostream>
#include <sstream>
#include <memory>

using namespace std;
class GameFactory{
public:
	~GameFactory();

	GameFactory();

	shared_ptr<AbstractPartsFactory> promptGameType();

private:
	static const int _TIC_TAC_TOE_KEY;

	static const map<unsigned int, string> _GAME_TYPES;

	shared_ptr<AbstractPartsFactory> generatePartsFactory(unsigned int gameType);
};

#endif