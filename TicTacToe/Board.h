#ifndef _BOARD_H_
#define _BOARD_H_

#include <vector>

using namespace std;

class Board {
public:
	virtual ~Board();

	virtual char getSpace(int index) = 0;

	virtual bool setSpace(int index, char piece) = 0;

	virtual vector<char> getSpaces() = 0;

	virtual void resetSpaces() = 0;

	virtual int getValidMaxLocation() = 0;

	virtual int getValidMinLocation() = 0;

protected:
	Board();
};

#endif