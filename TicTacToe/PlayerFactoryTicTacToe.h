#ifndef _PLAYER_FACTORY_TIC_TAC_TOE_H_
#define _PLAYER_FACTORY_TIC_TAC_TOE_H_

#include "PlayerFactory.h"
#include "HumanPlayerTicTacToe.h"
#include "CpuPlayerTicTacToe.h"

class PlayerFactoryTicTacToe: public PlayerFactory{
public:
	~PlayerFactoryTicTacToe();
	
	PlayerFactoryTicTacToe();

	shared_ptr<Player> generatePlayer();

private:
	static const vector<char> _VALID_PIECES;

	static const unsigned int _VALID_NAME_LENGTH;

	static const unsigned int _HUMAN_PLAYER_KEY;

	static const unsigned int _CPU_PLAYER_KEY;

	static const unsigned int _EASY_KEY;

	static const unsigned int _MEDIUM_KEY;

	static const unsigned int _HARD_KEY;

	static const unsigned int _MIN_VALID_PLAYER_TYPE;

	static const unsigned int _CHAR_LENGTH;

	static const string _DEFAULT_PLAYER_NAME;

	static const map<unsigned int, string> _VALID_PLAYER_TYPES;

	static const map<unsigned int, string> _DIFFICULTY_LEVELS;

	vector<char> _pickedPieces;

	string getPlayerName();

	char getPlayerPiece();

	unsigned int getPlayerType();

	unsigned int chooseDifficulty();
};

#endif