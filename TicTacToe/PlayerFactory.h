#ifndef _PLAYER_FACTORY_H_
#define _PLAYER_FACTORY_H_

#include <vector>
#include <map>
#include <algorithm>
#include "Player.h"
#include <memory>

using namespace std;

class PlayerFactory{
public:
	virtual ~PlayerFactory();

	virtual shared_ptr<Player> generatePlayer() = 0;

protected:
	PlayerFactory();
	
private:
	virtual string getPlayerName() = 0;

	virtual char getPlayerPiece() = 0;

	virtual unsigned int  getPlayerType() = 0;

};

#endif