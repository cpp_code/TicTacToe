/**@brief: engine that tracks and prompts player according to whose turn it is
**/
#include "TurnEngine.h"

TurnEngine::~TurnEngine(){}

TurnEngine::TurnEngine(shared_ptr<AbstractPartsFactory> &abstractPartsFactory){
	_board = abstractPartsFactory->createBoard();
	_view = abstractPartsFactory->createView();
	_winChecker = abstractPartsFactory->createWinChecker();
	_playerFactory = abstractPartsFactory->createPlayerFactory();
	_playerOne = _playerFactory->generatePlayer();
	_playerTwo = _playerFactory->generatePlayer();
	_playerTurn = _playerOne;
}

const unsigned int TurnEngine::_YES_KEY = 1;

const unsigned int TurnEngine::_NO_KEY = 2;

const unsigned int TurnEngine::_PLAYER_ONE_KEY = 1;

const unsigned int TurnEngine::_PLAYER_TWO_KEY = 2;

const int TurnEngine::_WINNER_CODE = 1;

const map<unsigned int, string> TurnEngine::_OPTIONS = {
	{_YES_KEY, "Yes"},
	{_NO_KEY, "No"}
};

/**@brief: get a player to take their turn, and then switch whos players turn it is
*
*@ret: location on the board that the player chose to move
**/
int TurnEngine::promptPlayer(){
	
	int location = 0;

	location = _playerTurn->placePiece(_board, _view);

	if(_playerTurn == _playerOne){
		_playerTurn = _playerTwo;
	} else {
		_playerTurn = _playerOne;
	}

	return location;

}

/**@brief: runs the game
*
* begins the game by resetting the board and the starting player
* lets the players take turns until the game is over
* lets players play again with the same or new pieces, or stop playing
**/
int TurnEngine::run(){
	int gameStatus;	

	do{
		gameStatus = _winChecker->getGameContinue();
		setStartingPlayer();
		_board->resetSpaces();

		while(!gameStatus){
			gameStatus = _winChecker->checkGameStatus(_board, promptPlayer());
		}

		evaluateWinner(gameStatus);
	}while(continuePlaying());


	return 0;
}

/**@brief: asks the players if they wish to play again
*
* finds out if the players want to play the same type of game again
*
* @ret: true if the users wish to play another round
* @ret: false if the users do not wish to play another round
**/
bool TurnEngine::promptPlayAgain(){
	int validResponse = 0;
	string userInput = " ";
	
	while (true) {
		cout << "Play Another Round?" << endl;
		for (auto const& it : _OPTIONS) {
			cout << it.first << ": " << it.second << endl;
		}
		
		ws(cin);
		getline(cin, userInput);

		// This code converts from string to number safely.
		stringstream userStream(userInput);
		if (userStream >> validResponse){
			if(validResponse == _YES_KEY){
				return true;
			} else if(validResponse == _NO_KEY){
				return false;
			}
		cout << "Invalid number, please try again" << endl;
		}
	}
}

/**@brief: asks if the players wish to switch their pieces
*
* @ret: true if the users wish to switch pieces
* @ret: false if the users do not wish to switch pieces
**/
bool TurnEngine::promptChangePieces(){
	int validResponse = 0;
	string userInput = " ";
	
	while (true) {
		cout << "Switch Player Pieces?" << endl;
		for (auto const& it : _OPTIONS) {
			cout << it.first << ": " << it.second << endl;
		}
		
		ws(cin);
		getline(cin, userInput);

		// This code converts from string to number safely.
		stringstream userStream(userInput);
		if (userStream >> validResponse){
			if (validResponse == _YES_KEY) {
				return true;
			}
			else if (validResponse == _NO_KEY) {
				return false;
			}
		cout << "Invalid number, please try again" << endl;
		}
	}
}

/**@brief switches players pieces
*
* swaps the pieces of player one and player two
**/
void TurnEngine::changePieces(){
	char tempPiece = _playerOne->getPiece();

	_playerOne->setPiece(_playerTwo->getPiece());
	_playerTwo->setPiece(tempPiece);

	cout << _playerOne->getName() << " is now " << _playerOne->getPiece() << endl;
	cout << _playerTwo->getName() << " is now " << _playerTwo->getPiece() << endl;
}

/**@brief asks if the user wishes to play again and change pieces
*
* finds out whether or not to play another round
* sets players pieces for the next round according to input
*
* @ret: true the users wish to play another game
* @ret: false if they do not wish to play another game
**/
bool TurnEngine::continuePlaying(){
	bool keepPlaying = promptPlayAgain();

	if(keepPlaying){
		if(promptChangePieces()){
			changePieces();
		}
	}

	return keepPlaying;

}

/**@brief sets the player to start the game
*
**/
void TurnEngine::setStartingPlayer(){
	int firstPlayer = 0;
	string userInput = " ";
	
	while (true) {
		cout << "Which player will go first?" << endl;
		cout << _PLAYER_ONE_KEY << ": " << _playerOne->getName() << endl;
		cout << _PLAYER_TWO_KEY << ": " << _playerTwo->getName() << endl;
		
		ws(cin);
		getline(cin, userInput);

		// This code converts from string to number safely.
		stringstream userStream(userInput);
		if (userStream >> firstPlayer){
			if(firstPlayer == _PLAYER_ONE_KEY){
				_playerTurn = _playerOne;
				break;
			} else if(firstPlayer == _PLAYER_TWO_KEY){
				_playerTurn = _playerTwo;
				break;
			}
		cout << "Invalid number, please try again" << endl;
		}
	}
}

/**@brief evaluates winners losers and ties
*
*evaluates who won, lost or tied
*updates the score
*displays the score
*
* @arg gameStatus: status on if the game has a winner or is just a tie
**/
void TurnEngine::evaluateWinner(int gameStatus){
	if(gameStatus == _WINNER_CODE){
		if(_playerTurn == _playerOne){
			scoreWinnerAndLoser(_playerTwo, _playerOne);
		} else {
			scoreWinnerAndLoser(_playerOne, _playerTwo);
		}
	} else {
		scoreTie();		
	}

	displayScore();
}

/**@brief updates the score of the winners and losers
*
*evaluates who won and lost
*updates the score
*
* @arg winningPlayer: player that won the game
* @arg losingPlayer: player that lost the game
**/
void TurnEngine::scoreWinnerAndLoser(shared_ptr<Player> &winningPlayer, shared_ptr<Player> &losingPlayer){
	_view->display(_board);
	cout << "Winner: " << winningPlayer->getName() << endl;
	winningPlayer->setWins(winningPlayer->getWins() + 1);
	losingPlayer->setLosses(losingPlayer->getLosses() + 1);
}

/**@brief updates the score of the tied players
*
*evaluates who tied
*updates the score
**/
void TurnEngine::scoreTie(){
	_view->display(_board);
	cout << "Tie Game" << endl;
	_playerOne->setTies(_playerOne->getTies() + 1);
	_playerTwo->setTies(_playerTwo->getTies() + 1);
}

/**@brief displays the players scores
**/
void TurnEngine::displayScore(){
	cout << _playerOne->getName() << ": | Wins: " << _playerOne->getWins() << " | Losses: " << _playerOne->getLosses() << " | Ties: " << _playerOne->getTies() << endl;
	cout << _playerTwo->getName() << ": | Wins: " << _playerTwo->getWins() << " | Losses: " << _playerTwo->getLosses() << " | Ties: " << _playerTwo->getTies() << endl;
}